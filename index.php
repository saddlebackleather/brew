<!DOCTYPE html>
<html lang="en">
<head>
	<title>Saddleback Systems Tools</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script src="js/Chart.min.js"></script>
	<script type="text/javascript">

        // Ajax Request to return load times of various URL's

        // @TODO Tie to current chart
        // @TODO Change Colors { < 3 Green, < 3.9 Warning, > 4 Danger }
		function updateLoadTime() {
			$.ajax({
					url: "collectData.php",
					dataType: "text",
					async: false
				})
				.done(function (text) {
					$('#loadTimediv').text('http://www.saddlebackleather.com loaded in ' + text + 's');
                    console.log('Successfully captured loadtime')
				});

		}
	</script>


</head>
	<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">SLC Tools</a>
			</div>
		</div><!-- /.container-fluid -->
	</nav>
		<div class="container">
			<div class="col-sm-1"></div>
			<div class="col-sm-10"><!-- Main Content Begin -->

				<select id="select_charttype">
					<option value="hourly">Hourly</option>
					<option value="daily">Daily</option>
				</select>
				<select id="select_url">
					<option value="http_www_saddlebackleather_com">Home Page</option>
					<option value="http_www_saddlebackleather_com_classic-briefcase">Classic Briefcase</option>
				</select>



				<h1>Loadspeed by Page</h1>
				<div id="loadTimediv" class="btn btn-primary btn-xs"></div>
				<div id="graph-container">
					<canvas id="myChart" width="1000" height="400"></canvas>
					<div id="myChartLegend"></div>
				</div>


			</div> <!-- Main Content End -->
			<div class="col-sm-1"></div>
		</div>
		<script type="text/javascript">
			//On Page Load
			$(document).ready(function() {

				//On dropdown select set the new url
				$("#select_url").change(function() {
					var charttype_value = $("#select_charttype").val();
					var url_value = $("#select_url").val();

					//pass the url to the draw function
					drawLineChart(url_value, charttype_value);
				});

				$("#select_charttype").change(function() {
					var charttype_value = $("#select_charttype").val();
					var url_value = $("#select_url").val();

					//pass the url to the draw function
					drawLineChart(url_value, charttype_value);
				});


				//Default Page Load
				//make URL for initial page load
				$url = "http_www_saddlebackleather_com";
				$charttype = "hourly";

				//call draw function on orig page load
				drawLineChart($url, $charttype);

				updateLoadTime();

			});


			//Kirk added variable here to be set from dropdown
			function drawLineChart($url, $charttype) {

				if ($charttype == "hourly") {
					// Hourly Chart
					var jsonData = $.ajax({
						url: '/json/' + $url + '.json',
						dataType: 'json',
					}).done(function (results) {

						var labels = ["00:00", "01:00", "02:00", "03:00", "04:00", "05:00",
								"06:00", "07:00", "08:00", "09:00", "10:00", "11:00",
								"12:00", "13:00", "14:00", "15:00", "16:00", "17:00",
								"18:00", "19:00", "20:00", "21:00", "22:00", "23:00"],
							data = [results['00'], results['01'], results['02'], results['03'],
								results['04'], results['05'], results['06'], results['07'],
								results['08'], results['09'], results['10'], results['11'],
								results['12'], results['13'], results['14'], results['15'],
								results['16'], results['17'], results['18'], results['19'],
								results['20'], results['21'], results['22'], results['23']];

						// Create the chart.js data structure using 'labels' and 'data'
						var tempData = {
							labels: labels,
							datasets: [{
								label: $url,
								fillColor: "rgba(151,187,205,0.2)",
								strokeColor: "rgba(151,187,205,1)",
								pointColor: "rgba(151,187,205,1)",
								pointStrokeColor: "#fff",
								pointHighlightFill: "#fff",
								pointHighlightStroke: "rgba(151,187,205,1)",
								data: data
							}]
						};

						// Get the context of the canvas element we want to select
						var ctx = $("#myChart").get(0).getContext("2d");

						if (typeof myLineChart !== 'undefined') {
							myLineChart.destroy();
						}

						// Instantiate a new chart
						var myLineChart = new Chart(ctx).Line(tempData, {
							responsive: true,
							legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"color:<%=datasets[i].pointColor%>\"><i class=\"glyphicon glyphicon-stats\"> </i> </span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
						});

						var legend = myLineChart.generateLegend();
						$("#myChartLegend").html(legend);
					});
				} else {

					// Add a helper to format timestamp data
					Date.prototype.formatMMDDYYYY = function() {
						return (this.getMonth() + 1) +
							"/" +  this.getDate() +
							"/" +  this.getFullYear();
					};

					// Dialy Chart
					var jsonData = $.ajax({
						url: '/json/' + $url + '_daily.json',
						dataType: 'json',
					}).done(function (results) {

						var labels = [],data = [];
							$.each(results, function(k, v) {
								labels.push(k);
								data.push(v);
							});

						// Create the chart.js data structure using 'labels' and 'data'
						var tempData = {
							labels: labels,
							datasets: [{
								label: $url,
								fillColor: "rgba(151,187,205,0.2)",
								strokeColor: "rgba(151,187,205,1)",
								pointColor: "rgba(151,187,205,1)",
								pointStrokeColor: "#fff",
								pointHighlightFill: "#fff",
								pointHighlightStroke: "rgba(151,187,205,1)",
								data: data
							}]
						};

						// Get the context of the canvas element we want to select
						var ctx = $("#myChart").get(0).getContext("2d");

						if (typeof myLineChart !== 'undefined') {
							myLineChart.destroy();
						}

						// Instantiate a new chart
						var myLineChart = new Chart(ctx).Line(tempData, {
							responsive: true,
							legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"color:<%=datasets[i].pointColor%>\"><i class=\"glyphicon glyphicon-stats\"> </i> </span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
						});

						var legend = myLineChart.generateLegend();
						$("#myChartLegend").html(legend);
					});
				}
			}

		</script>
	</body>
</html>