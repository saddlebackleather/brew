<?php
/**
 * Created by PhpStorm.
 * User: matt
 * Date: 4/4/16
 * Time: 12:30 AM
 */

function debug($object){
    echo "<pre>";
    print_r($object);
    echo "</pre>";
}

function doAnalysis($url){
    $fp = "/vagrant/src/json/$url.json";

    $json = json_decode(file_get_contents($fp), true);

    $total = 0;

    $i = 0;
    foreach($json as $loadspeed) {
        $total += $loadspeed;

        $i++;
    }

    $average = $total / $i;

    return $average;
}

function recordLogs($urls, $logtype)
{
    foreach ($urls as $url) {

        // Replace special characters ://, /, and .
        $url = preg_replace('/(\:\/\/)|(\/)|(\.)/', '_', $url);

        if ($logtype == "daily") {

            $fp = "/vagrant/src/json/$url" . "_daily.json";

            $json = json_decode(file_get_contents($fp), true);

            $daily_average = round(doAnalysis($url), 3, PHP_ROUND_HALF_UP);

            $json[date("m-d-y")] = $daily_average;

            echo $daily_average;

            $write = fopen($fp, 'w');
            fwrite($write, json_encode($json));
            fclose($write);

        } else {
            // Hourly
            // Run phantom and get the loadspeed
            $execute = "/vagrant/src/bin/phantomjs /vagrant/src/js/loadspeed.js $url";

            $loadspeed = exec($execute);

            $fp = "/vagrant/src/json/$url.json";
            $json = json_decode(file_get_contents($fp), true);

            $json[date("H")] = $loadspeed;

            $write = fopen($fp, 'w');
            fwrite($write, json_encode($json));
            fclose($write);

        }
    }
}

##################################################
#
# Main Code
#
##################################################

// Check if run from command line
if (php_sapi_name() == "cli") {

    $urls = array("http://www.saddlebackleather.com");

    switch ($argv[1]) {
        case "daily":
            recordLogs($urls, "daily");
            break;
        case "hourly":
            recordLogs($urls, "hourly");
            break;
    }

} else {

    $request = exec("/vagrant/src/bin/phantomjs /vagrant/src/js/loadspeed.js http://www.saddlebackleather.com");

    echo $request;

}




?>